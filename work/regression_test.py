'''
Initial development by Joe Taylor

Regression test framework using pytest:

Each test_config_x() runs a test comparing the output of hamx to a numpy template array:
For this purpose I made a test array "test.npy" which is included on the repo. In the 
full set of regression tests, .npy arrays could be made for each test easily using numpy.save().

Each config_x() runs hamx with different parameter settings

Each individual test stores all generated files in "fol". If the test passes, that folder is automatically
removed. If the test fails, all outputs will remain.

'''

def cleanup(fol):
  import os  
  from shutil import rmtree
  # remove entire working directory	
  if os.path.isdir(fol):
    rmtree(fol)

def test_config_1():
  fol = 'test.out/config_1'
  import numpy as np
  testing = np.load('test.npy')
  diff_array = config_1(fol) - testing
  assert diff_array.var() < 1e11
  cleanup(fol)
    
def test_config_2():
  fol = 'test.out/config_2'
  import numpy as np
  testing = np.load('test.npy')
  diff_array = config_2(fol) - testing
  assert diff_array.var() < 1e5
  cleanup(fol)

def test_config_3():
  fol = 'test.out/config_3'
  import numpy as np
  testing = np.load('test.npy')
  diff_array = config_3(fol) - testing
  assert diff_array.var() < 1e9
  cleanup(fol)

def test_config_4():
  fol = 'test.out/config_4'
  import numpy as np
  testing = np.load('test.npy')
  diff_array = config_4(fol) - testing
  assert diff_array.var() < 1e10
  cleanup(fol)   
    
def config_1(wrkdir):  
  import sys
  sys.path.append('/Users/tjtaylo2/Software/hammurabi_x/hamx/src/hampyx') # path to hamx files 
  sys.path.append('/Users/tjtaylo2/Software/hammurabi/hammurabi/hampy') # path to hammurabi files
  import hampyx_class as hampyx
  import numpy as np

  hamx = hampyx.Hampy(custom_parameters = 'params_template.xml',working_directory = wrkdir)
  hamx.mod_par([['Obsout','Sync'],'filename',wrkdir+'/iqu_sync_23.fits'])
  hamx.mod_par([['Obsout','DM'],'filename',wrkdir+'/dm.fits'])
  hamx.mod_par([['Obsout','Faraday'],'filename',wrkdir+'/fd.fits'])
  hamx.call(keychain=[[['MagneticField','Regular'],'type','WMAP'],
                      [['MagneticField','Random'],'cue','0'],
                      [['FreeElectron','Regular'],'type','YMW16'],
                      [['FreeElectron','Random'],'cue','0'],
                      [['CRE'],'type','Analytic']])
  hamx_maps = hamx.get_obs()
  return hamx_maps['Sync']['I']

def config_2(wrkdir):  
  import hampyx_class as hampyx
  import numpy as np

  hamx = hampyx.Hampy(custom_parameters = 'params_template.xml',working_directory = wrkdir)
  hamx.mod_par([['Obsout','Sync'],'filename',wrkdir+'/iqu_sync_23.fits'])
  hamx.mod_par([['Obsout','DM'],'filename',wrkdir+'/dm.fits'])
  hamx.mod_par([['Obsout','Faraday'],'filename',wrkdir+'/fd.fits'])
  hamx.call(keychain=[[['MagneticField','Regular'],'type','Jaffe'],
                      [['MagneticField','Random'],'cue','0'],
                      [['FreeElectron','Regular'],'type','Verify'],
                      [['FreeElectron','Random'],'cue','0'],
                      [['CRE'],'type','Verify']])
  hamx_maps = hamx.get_obs()
  return hamx_maps['Sync']['I']

def config_3(wrkdir):  
  import hampyx_class as hampyx
  import numpy as np

  hamx = hampyx.Hampy(custom_parameters = 'params_template.xml',working_directory = wrkdir)
  hamx.mod_par([['Obsout','Sync'],'filename',wrkdir+'/iqu_sync_23.fits'])
  hamx.mod_par([['Obsout','DM'],'filename',wrkdir+'/dm.fits'])
  hamx.mod_par([['Obsout','Faraday'],'filename',wrkdir+'/fd.fits'])
  hamx.call(keychain=[[['MagneticField','Regular'],'type','Verify'],
                      [['MagneticField','Random'],'cue','0'],
                      [['FreeElectron','Regular'],'type','Verify'],
                      [['FreeElectron','Random'],'cue','1'],
                      [['CRE'],'type','Verify']])
  hamx_maps = hamx.get_obs()
  return hamx_maps['Sync']['I']

def config_4(wrkdir):  
  import hampyx_class as hampyx
  import numpy as np

  hamx = hampyx.Hampy(custom_parameters = 'params_template.xml',working_directory = wrkdir)
  hamx.mod_par([['Obsout','Sync'],'filename',wrkdir+'/iqu_sync_23.fits'])
  hamx.mod_par([['Obsout','DM'],'filename',wrkdir+'/dm.fits'])
  hamx.mod_par([['Obsout','Faraday'],'filename',wrkdir+'/fd.fits'])
  hamx.call(keychain=[[['MagneticField','Regular'],'type','Verify'],
                      [['MagneticField','Random'],'cue','1'],
                      [['FreeElectron','Regular'],'type','Verify'],
                      [['FreeElectron','Random'],'cue','0'],
                      [['CRE'],'type','Verify']])
  hamx_maps = hamx.get_obs()
  return hamx_maps['Sync']['I']

      
      
      
      
